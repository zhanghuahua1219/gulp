// 1. 引入相关模块
const express = require('express')
const fs = require('fs')
const multer = require('multer')
// （0.1）调用multer进行配置
const uploader = multer({
    dest: 'upload/' // 定义文件上传后保存的文件夹
})

const app = express()
// 2. 使用中间件
// 2.1 托管静态页面
app.use('/node_modules', express.static('./node_modules'))
app.use('views', express.static('./views'))
// 3. 监听
// 3.1 首页
app.get('/', (req, res) => {
    // console.log(res)
    res.sendfile('./views/index.html')
})
// 3.2 单文件上传接口 （0.2）传文件接口中使用中间件 file一定要和表单元素中的name保持一致
app.post('/singlefileupload', uploader.single('photo'), (req, res) => {
    // （0.3） 通过file属性获取上传的内容
    let file = req.file
    console.log(req)
    let fileInfo = {}
    // 获取文件信息
    fileInfo.mimetype = file.mimetype         // 获取文件类型
    fileInfo.originalname = file.originalname // 文件原始名称
    fileInfo.size = file.size // 文件大小
    fileInfo.path = file.path // 文件上传后保存的路径
    res.json(fileInfo)
})
// 3.3 多文件上传使用 uploader.array(文件字段,数量限制)
app.post('/arrayfileupload', uploader.array('photos', 3), (req, res) => {
    let files = req.files
    console.log(files)
    let fileInfos = []
    // 获取文件信息
    for (let i in files) {
        var file = files[i]
        var fileInfo = {}
        // 分别设置 1.文件类型 2.文件原始名称 3.文件大小 4.文件上传后保存的路径
        fileInfo.mimetype = file.mimetype
        fileInfo.originalname = file.originalname
        fileInfo.size = file.size
        fileInfo.path = file.path

        fileInfos.push(fileInfo)
    }
    res.json(files)
})

// 4. 启动服务
app.listen(99, () => {
    console.log('http://localhost:99 is running')
})