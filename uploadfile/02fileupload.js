// 1. 引入相关模块
const express = require('express')
const fs = require('fs')
const app = express()

const multer = require('multer')
let storage = multer.diskStorage({
    destination: './upload',      // 文件夹位置
    filename: (req, file, cb) => { // 文件名称
        cb(null, Date.now() + '-' + file.originalname);
    }
});

// 调用multer进行配置
const uploader = multer({
    storage: storage
})

// 2. 使用中间件
// 2.1 托管静态页面
app.use('/node_modules', express.static('./node_modules'))
app.use('views', express.static('./views'))
app.use('/upload', express.static('./upload'))

// 3. 监听
// 3.1 首页
app.get('/', (req, res) => {
    // console.log(res)
    res.sendfile('./views/index.html')
})
// 3.1 上传文件接口中使用中间件 file一定要和表单元素中的name保持一致
// 单文件上传 uploader.single(文件字段)
app.post('/singlefileupload',uploader.single('photo'), (req, res) => {
    // 3. 通过file属性获取上传的内容
    var file = req.file;
    var fileInfo = {};

    // 获取文件信息
    fileInfo.mimetype = file.mimetype; // 文件类型
    fileInfo.originalname = file.originalname; // 文件原始名称
    fileInfo.size = file.size; // 文件大小
    fileInfo.path = file.path; // 文件上传后保存的路径

    res.json(fileInfo);
})

// 4. 启动服务
app.listen(99, () => {
    console.log('http://localhost:99 is running')
})