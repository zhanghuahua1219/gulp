// 1. 定义一个方法
function add (a, b) {
    console.log(a + b)
}

// 2. 导出一个对象
export default {
    add  // 等价与add: add ==》add: function(a, b) {// ...}
}