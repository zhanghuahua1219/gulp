/* 依次读取files文件中的01.txt 02.txt 03.txt中的内容 */
const fs = require('fs')
const path = require('path')
// 1. 传统方式实现 成功读取01.txt之后在 读取02.txt  .... 回调中在嵌套回调
function readSortFiles(pathUrl, callback) {
    // 1.1 读取第一个文件
    fs.readFile(pathUrl,'utf8', (err, results) => {
        // 1.1.1 失败
        if (err) throw err
        // 1.1.2 成功
        callback && callback(results)
    })
}
// 1.2 调用
/* readSortFiles(path.join(__dirname, './files/01.txt'), function(results) {
    // 1.2.1 成功读取第一个文件后 先打印 在读取第二个文件
    console.log(results)
    readSortFiles(path.join(__dirname, './files/02.txt'), function(results) {
        // 1.2.2 成功读取第er个文件后 先打印 在读取第san个文件
        console.log(results)
        readSortFiles(path.join(__dirname, './files/03.txt'), function(results) {
            // 1.2.3 成功读取第3个文件后 先打印 在读取第..个文件
            console.log(results)
        })
    })
}) */


// 2. 使用Promise实现解决嵌套太深的问题
function readFilePromise (pathUrl) {
    // 2.1 创建一个Promise的实例
    var p = new Promise(function(resolve, reject) { // resolve成功回调函数  reject失败回调函数
        // 2.1.1 读取文件
        fs.readFile(pathUrl, 'utf8', (err, results) => {
            // 2.1.2 失败
            if(err) throw err
            // 2.1.3 成功
            resolve(results)
        })
    })
    // 2.2 返回promise实例
    return p
}
readFilePromise(path.join(__dirname, './files/01.txt'))
.then(results => {
    console.log(results)

    // 2.3 在返回promise实例 可以继续then
    return readFilePromise(path.join(__dirname, './files/02.txt'))
})
.then(results => {
    console.log(results)
    // 2.4 在返回promise实例 可以继续then
    return readFilePromise(path.join(__dirname, './files/03.txt'))
})
.then(results => {
    console.log(results)
})